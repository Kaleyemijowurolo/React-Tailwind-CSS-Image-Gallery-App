import React from "react";

const Footer = () => {
  const [year, setYear] = React.useState("");

  React.useEffect(() => setYear(new Date().getFullYear()), []);
  return (
    <div className="w-full p-20 text-center">
      &copy; {"Kayode Kaleyemi"}, {year} | {"@TailawindCSS"}.
    </div>
  );
};

export default Footer;
