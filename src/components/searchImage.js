import React, { useState } from "react";

const SearchImage = ({ searchText }) => {
  const [text, setText] = useState();

  const onSubmit = (e) => {
    e.preventDefault();
    searchText(text);
  };
  return (
    <div className="max-w-sm rounded overflow-hidden my-10 mx-auto">
      <form onSubmit={onSubmit} className="w-full max-w-sm">
        <div className="flex items-center border-b border-teal-500 p-2">
          <input
            onChange={(e) => setText(e.target.value)}
            type="text"
            className="appearance-none bg-transparent border-none w-full text-gray-200 mr-3 py-1 px-2 leading-tight focus-within:outline-none placeholder-gray-200"
            placeholder="Search image term..."
          />
          <button
            className="flex-shrink-0 bg-teal-500 hover:bg-gray-100 border-teal-500 hover:border-teal-700 text-lg border-none py-1 px-2 rounded focus:outline-none"
            type="submit"
          >
            🔍
          </button>
        </div>
      </form>
    </div>
  );
};

export default SearchImage;
