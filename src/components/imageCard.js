import React from "react";

const ImageCard = ({ image }) => {
  const tags = image.tags.split(",");
  return (
    <div className="max-w-sm rounded-lg overflow-hidden shadow-lg hover:opacity-80">
      <img src={image.webformatURL} alt="" className="w-full" />
      <div className="px-6 py-4">
        <div className="font-bold text-blue-800 text-xl mb-2">
          Photo by {image.user}
        </div>
        <ul>
          <li>
            <strong>Views:</strong> {image.views}
          </li>
          <li>
            <strong>Downloads:</strong> {image.downloads}
          </li>
          <li>
            <strong>Likes:</strong> {image.likes}
          </li>
        </ul>
      </div>
      <div className="px-6 py-4">
        {tags.map((tag, idx) => (
          <span
            key={image.id + idx}
            className="inline bg-gray-100 rounded-full px-3 text-sm font-semibold text-gray-400 mx-2"
          >
            #{tag}
          </span>
        ))}
      </div>
    </div>
  );
};

export { ImageCard };
